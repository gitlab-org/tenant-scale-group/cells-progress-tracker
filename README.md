## Cells Progress Tracker

Displays some metrics around Cells' work, via scheduled CI pipelines running every 1 hour, publishing via GitLab pages.

### Schema Migration

- Dashboard tracking the progress of the schema migration work related to [multiple databases](https://docs.gitlab.com/ee/development/database/multiple_databases.html#gitlab-schema).
- Automatically updated via CI.
- https://gitlab-org.gitlab.io/tenant-scale-group/cells-progress-tracker/schema_migration

### Sharding Keys

- Dashboard tracking the progress of the sharding key migration work related to [Organization Isolation](https://gitlab.com/groups/gitlab-org/-/epics/11670).
- Automatically updated via CI.
- https://gitlab-org.gitlab.io/tenant-scale-group/cells-progress-tracker/sharding_keys

### Cells 1.0 (MOVED)

- Dashboard tracking the progress of the [Cells 1.0 epic](https://gitlab.com/groups/gitlab-org/-/epics/12383).
- Automatically updated via CI.
- Previously at https://gitlab-org.gitlab.io/tenant-scale-group/cells-progress-tracker/cells_10, now redirected to https://epic-dashboard-gitlab-org-tenant-scale-group-4aecf10d1d02154641.gitlab.io/epic_12383.
