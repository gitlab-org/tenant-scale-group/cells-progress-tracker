require 'yaml'
require 'erb'
require 'json'
require_relative 'helper.rb'

Dir.chdir "gitlab"


def table_growth_data
  puts Dir.pwd
  data = File.readlines('../scripts/sharding_keys/table-growth.txt').map(&:chomp)

  # Parse data into a format suitable for Chart.js
  parsed_data = data.map do |line|
    date, count = line.split(': ')
    { date: date, count: count.to_i }
  end
end

def html
  template = %{
  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta name="generator" content="GitLab Pages">
      <title>Tenant Scale Progress - Sharding Keys</title>
      <link rel="stylesheet" href="style.css">
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
      <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/moment@2.29.1/moment.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment@0.1.1"></script>
      <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
    </head>
    <body>
      <div class="dropdown" style="position: fixed; top: 10px; right: 10px;">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Links
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <!-- Dropdown Items (Links) -->
          <a class="dropdown-item" href="/schema_migration" target="_blank">Schema migration progress</a>
          <a class="dropdown-item" href="/sharding_keys">Sharding key migration progress</a>
          <a class="dropdown-item" href="/cells_10" target="_blank">Cells 1.0 progress</a>
          <a class="dropdown-item" href="https://gitlab.com/gitlab-org/tenant-scale-group/cells-progress-tracker" target="_blank">Contribute</a>
        </div>
      </div>

      <div class="content-container">

        <h1>Progress of Sharding Key migration work for Cells</h1>

        <small>
          This dashboard is used to track progress of the sharding key migration work related to <a href=https://gitlab.com/groups/gitlab-org/-/epics/11670>Organization Isolation</a><br>
          <a href=https://gitlab.com/gitlab-org/gitlab/-/issues/429184>Issue</a>

          <br>
          Last updated at: #{Time.now.utc}
        </small>

        <h3>  Progress </h3>

          <h5> Total number of tables requiring sharding key/desired sharding key: <%= total_progress_data.total_tables %> </h5>
            <small> This includes tables with schemas in one of: gitlab_main, gitlab_main_cell, gitlab_ci, gitlab_sec and are NOT</small>
            <small> JiHu specific tables or explicitly exempted using `exempt_from_sharding: true` in their dictionary file. </small>
          <h5> Total number of tables having sharding key/desired sharding key set: <%= total_progress_data.completed_tables %></h5>
          <p> Percentage of work completed </p>

          <div class="progress w-25" role="progressbar">
            <div class="progress-bar bg-success" style="width: <%=total_progress_data.percentage_completed%>%"><%= total_progress_data.percentage_completed %>%</div>
          </div>

          <br>

          <div style="text-align: center;">
            <div style="width: 500px; height: 200px;">
              <canvas id="piechart"></canvas>
            </div>
          </div>

        <h3> Progress over time </h3>
          <small> This graph shows the growth of number of tables having sharding key data set over time</small>
          <div style="text-align: center;">
            <div style="width: 500px; height: 350px;">
              <canvas id="table_growth"></canvas>
            </div>
          </div>

          <br>

          <h3 id="issues">sharding_key_issue_url progress tracker </h3>

          <small>We have setup an issue for each table that needs further work. This is denoted by `sharding_key_issue_url` </small>
          <br>
          <small>This part of the dashboard exposes these issues and their progress</small>
          <br>
          <small>We originally started with a total of 68 unique issues.</small>
          <h5> Current unique issues total: <%= data_grouped_by_feature_category.flat_map { |feature_category_data| feature_category_data.entries.map(&:sharding_key_issue_url) }.compact.uniq.count %> </h5>
          <h5> Total tables listed: <%= data_grouped_by_feature_category.flat_map { |feature_category_data| feature_category_data.entries.map(&:sharding_key_issue_url) }.compact.count %> </h5>

          <% overall_index = 1 %>
          <% names = [] %>
          <small>
            <table>
              <tr>
                <th>No</th> 
                <th>Table name</th>
                <th>Issue</th>
                <th>Feature category</th>
                <th>Assigned group</th>
                <th>Workflow label</th>
                <th>Assignee</th>
                <th>Status</th>
              </tr>
              <% data_grouped_by_feature_category.each do |feature_category_data|  %>
                <% feature_category_data.entries.select { |entry| entry.sharding_key_issue_url.present? }.each do |entry| %>
                  <% names << entry.table_name %>
                  <tr>
                    <td><%= overall_index %></td>
                    <td><%= entry.full_link %></td>
                    <td><a href=<%=entry.sharding_key_issue_url %>>Link</a></td>
                    <td><%= feature_category_data.feature_category_name %></td>
                    <td><%= entry.sharding_key_issue_url_data.labels.find { |label| label.start_with?('group') } %></td>
                    <td><%= entry.sharding_key_issue_url_data.labels.find { |label| label.start_with?('workflow') } %></td>
                    <td><%= entry.sharding_key_issue_url_data.assignees.map(&:username).join(', ') %></td>
                    <td><%= entry.sharding_key_issue_url_data.state %></td>
                  </tr>
                  <% overall_index += 1 %>
                <% end %>
              <% end %>
              <%= puts "DEBUG: tables with sharding_key_issue_url present" %>
              <% names.sort.each do |table_name| %>
                <%= puts table_name %>
              <% end %>
              <%= puts "DEBUG: end of tables with sharding_key_issue_url present" %>
            </table>
          </small>
          <br>


        <h3 id="missing"> Tables missing sharding key/desired sharding key AND sharding_key_issue_url </h3>
          <% tables_needing_sharding = data_grouped_by_feature_category.flat_map { |pfc| pfc.entries.select { |entry| entry.schema_needs_sharding? } } %>
          <% selection = tables_needing_sharding.select {|entry| entry.sharding_key.nil? && entry.desired_sharding_key.nil? && entry.sharding_key_issue_url.nil?  } %>
          # Sometimes desired sharding key configuration is missing, but the backfill migration has been run, and such tables are valid.
          <% tables_needing_sharding_and_missing_sharding_key_data_and_issue_url = selection.reject {|entry| entry.desired_sharding_key_migration_job_name.present?  } %>

          <table>
            <tr>
              <th>Table Name</th>
              <th>Feature Category</th>
            </tr>
            <% tables_needing_sharding_and_missing_sharding_key_data_and_issue_url.each do |entry| %>
              <tr>
                <td><%= entry.full_link %></td>
                <td><%= entry.feature_categories.first %></td>
              </tr>
            <% end %>
          </table>
          
        <br>

        <h3 id="desired_sharding_key"> Desired sharding key migration progress </h3>

        <small>We can setup `NOT NULL` constraint on the sharding key column only after the backfill migrations have been run and finalized. </small>
        <br>
        <small>Current blocker: Backfill migrations can only be finalized after the next required stop, which is milestone 17.5</small>

        <small>
         <table>
          <tr>
            <th>No</th> 
            <th>Table name</th>
            <th>Feature category</th>
            <th>Backfill migration run?</th>
            <th>Backfill migration finalized?</th>
            <th>NOT NULL constraint added?</th>
          </tr>
          <% entries = data_grouped_by_feature_category.flat_map { |feature_category_data| feature_category_data.entries.select { |entry| (entry.desired_sharding_key.present? && !entry.desired_sharding_key.first[1]['awaiting_backfill_on_parent']) || (entry.desired_sharding_key.nil? && entry.desired_sharding_key_migration_job_name.present?) } } %>
          <% entries.sort_by{ |entry| entry.desired_sharding_key_migration_job_name.present? ? 0 : 1 }.each.with_index(1) do |entry, index| %>
              <tr>
                <td><%= index %></td> 
                <td><%= entry.full_link %></td>
                <td><%= entry.feature_categories.first %></td>
                <td><%= entry.desired_sharding_key_migration_job_name.present? ? 'Yes'  : 'No' %></td>
                <td><%= entry.backfill_migration_finalized_string %></td>
                <td><%= entry.not_null_constraint_added_string %></td>
              </tr>
            <% end %>
          </table>
        </small>

        <br>

        <h3 id="desired_sharding_ke_awaiting_backfill_on_parent"> Tables waiting for backfill and `NOT NULL` constraint creation on parent table to finish </h3>

        <small>For these tables, the backfill migrations can be generated, </small>
        <br>
        <small>only after the backfill migrations of the parent table have been completed, and the `NOT NULL` constraint has been added to the sharding key column of the parent. </small>
        <br>
        <small>This is denoted by `awaiting_backfill_on_parent: true` in the `.yml` file. </small>

        <small>
        <table>
          <tr>
            <th>No</th> 
            <th>Table name</th>
            <th>Feature category</th>
            <th>Backfill migration run?</th>
            <th>Backfill migration finalized?</th>
            <th>NOT NULL constraint added?</th>
          </tr>
          <% entries = data_grouped_by_feature_category.flat_map { |feature_category_data| feature_category_data.entries.select { |entry| entry.desired_sharding_key.present? && entry.desired_sharding_key.first[1]['awaiting_backfill_on_parent'] } } %>
          <% entries.sort_by{ |entry| entry.desired_sharding_key_migration_job_name.present? ? 0 : 1 }.each.with_index(1) do |entry, index| %>
              <tr>
                <td><%= index %></td> 
                <td><%= entry.full_link %></td>
                <td><%= entry.feature_categories.first %></td>
                <td><%= entry.desired_sharding_key_migration_job_name.present? ? 'Yes'  : 'No' %></td>
                <td><%= entry.desired_sharding_key_migration_job_name.present? ? 'Needs manual check'  : 'Migration needs to be generated & run first' %></td>
                <td><%= entry.not_null_constraint_added_string %></td>
              </tr>
            <% end %>
          </table>
        </small>

        <br>

        <h3> Total progress per feature category </h3>
        <br>
        <small>
          <% data_grouped_by_feature_category.each do |feature_category_data|  %>
            <details><summary> <%= feature_category_data.feature_category_name %> - <%= feature_category_data.completed_string %> <%= feature_category_data.emoji %> </summary>

            <table>
              <tr>
                <th>Number</th>
                <th>Table name</th>
                <th>gitlab_schema</th>
                <th>Sharding key</th>
                <th>Desired sharding key</th>
                <th>Work complete?</th>
                <th>Work to be done</th>
              </tr>

              <% feature_category_data.entries.each.with_index(1) do |data, index|  %>
                <tr>
                  <td><%= index %></td>
                  <td><%= data.full_link %></td>
                  <td><%= data.gitlab_schema %></td>
                  <td><%= data.sharding_key_key_name %></td>
                  <td><%= data.desired_sharding_key_key_name %></td>
                  <td><%= data.sharding_key_work_complete? ? "Yes" : "No" %></td>
                  <td><%= data.sharding_key_work_to_be_done_string %></td>
                </tr>
              <% end %>
            </table>
            </details>
          <% end  %>
        </small>
      </div>

      <script>
        var percentage_a = <%= percentage(total_progress_data.completed_tables, total_progress_data.total_tables) %>;
        var percentage_b = <%= percentage(total_progress_data.incomplete_tables, total_progress_data.total_tables) %>;

        var ctx = document.getElementById("piechart").getContext("2d");
        var pieChart = new Chart(ctx, {
          type: "pie",
          height: 50,
          width: 40,
          data: {
            labels: ["sharding key/desired sharding key set", "no sharding key"],
            datasets: [
              {
                data: [percentage_a, percentage_b],
                backgroundColor: ["#34a853", "#EA4335"],
              },
            ],
          },
        });

        var data = <%= table_growth_data.to_json %>

        var dates = data.map(item => new Date(item.date));
        var counts = data.map(item => item.count);

        var table_growth_ctx = document.getElementById("table_growth").getContext("2d");

        const myChart = new Chart(table_growth_ctx, {
            type: "line",
            data: {
                labels: dates, // X-axis data (dates)
                datasets: [{
                    label: "Counts",
                    data: counts, // Y-axis data (counts)
                    borderColor: "rgb(75, 192, 192)", // Line color
                    fill: false, // Don't fill the area under the line
                }]
            },
            options: {
                scales: {
                    x: {
                        type: 'time',
                        time: {
                            unit: 'month',
                            displayFormats: {
                              month: 'MMM YYYY' // Customize the date format
                          },
                        },
                    },
                    y: {
                        beginAtZero: false,
                    }
                }
            }
        });
      </script>
    </body>
  </html>
  }.gsub(/^  /, '')

  rhtml = ERB.new(template)
  rhtml.result
end

File.open('../public/sharding_keys.html', 'w+') { |file| file.write(html) }

Dir.chdir "."
