require 'gitlab'

Gitlab.configure do |config|
  config.endpoint      = "https://gitlab.com/api/v4"
  config.private_token = ENV["GIT_PUSH_TOKEN"]
end
