require 'yaml'
require 'erb'
require 'json'
require_relative 'workstreams_by_workflow.rb'


def issue_growth_data
  data = File.readlines('scripts/cells_10/issue-count-growth.txt').map(&:chomp)

  # Parse data into a format suitable for Chart.js
  parsed_data = data.map do |line|
    date, counts = line.split(': ')
    counts = counts.split(",")

    { date: date, total_count: counts[1].to_i, closed_count: counts[0].to_i }
  end
end

def grouped_data
  @grouped_data ||= grouped_issues_data
end

def grouped_data_all
  @grouped_data_all ||= grouped_issues_data(only_open: false)
end

def workflow_column_names
  return @names if @names.present?

  @names = []
  grouped_data_all.each do |epic_data|
    epic_data.each do |epic_iid, value|
      @names << value[:grouped_issues].keys
    end
  end

  @names = @names.flatten(1).uniq.sort
end

def grouped_per_department_data
  return @grouped_per_department_data if @grouped_per_department_data.present?

  @grouped_per_department_data = {}

  all_recursive_workstream_issues.each do |issue|
    section_label = issue.labels.find {|label| label.start_with?("section::") } || 'no section'

    @grouped_per_department_data[section_label] ||= {}
    workflow_label = issue.labels.find {|label| label.start_with?("workflow::")} || 'no workflow label'

    @grouped_per_department_data[section_label][workflow_label] ||= []
    @grouped_per_department_data[section_label][workflow_label] << issue
  end

  @grouped_per_department_data
end

def grouped_per_department_data_all
  return @grouped_per_department_data_all if @grouped_per_department_data_all.present?

  @grouped_per_department_data_all = {}

  all_recursive_workstream_issues(only_open: false).each do |issue|
    section_label = issue.labels.find {|label| label.start_with?("section::") } || 'no section'

    @grouped_per_department_data_all[section_label] ||= {}
    workflow_label = issue.labels.find {|label| label.start_with?("workflow::")} || 'no workflow label'

    @grouped_per_department_data_all[section_label][workflow_label] ||= []
    @grouped_per_department_data_all[section_label][workflow_label] << issue
  end

  @grouped_per_department_data_all
end

def grouped_per_group_data
  return @grouped_per_group_data if @grouped_per_group_data.present?

  @grouped_per_group_data = {}

  all_recursive_workstream_issues.each do |issue|
    group_label = issue.labels.find {|label| label.start_with?("group::") } || 'no group'

    @grouped_per_group_data[group_label] ||= {}
    workflow_label = issue.labels.find {|label| label.start_with?("workflow::")} || 'no workflow label'

    @grouped_per_group_data[group_label][workflow_label] ||= []
    @grouped_per_group_data[group_label][workflow_label] << issue
  end

  @grouped_per_group_data
end

def grouped_per_group_data_all
  return @grouped_per_group_data_all if @grouped_per_group_data_all.present?

  @grouped_per_group_data_all = {}

  all_recursive_workstream_issues(only_open: false).each do |issue|
    group_label = issue.labels.find {|label| label.start_with?("group::") } || 'no group'

    @grouped_per_group_data_all[group_label] ||= {}
    workflow_label = issue.labels.find {|label| label.start_with?("workflow::")} || 'no workflow label'

    @grouped_per_group_data_all[group_label][workflow_label] ||= []
    @grouped_per_group_data_all[group_label][workflow_label] << issue
  end

  @grouped_per_group_data_all
end

def grouped_per_milestone_data
  return @grouped_per_milestone_data if @grouped_per_milestone_data.present?

  @grouped_per_milestone_data = {}

  all_recursive_workstream_issues.each do |issue|
    milestone_label = if issue.milestone
      issue.milestone.title
    else
      'no milestone'
    end

    @grouped_per_milestone_data[milestone_label] ||= {}
    workflow_label = issue.labels.find {|label| label.start_with?("workflow::")} || 'no workflow label'

    @grouped_per_milestone_data[milestone_label][workflow_label] ||= []
    @grouped_per_milestone_data[milestone_label][workflow_label] << issue
  end

  @grouped_per_milestone_data
end

def grouped_per_milestone_data_all
  return @grouped_per_milestone_data_all if @grouped_per_milestone_data_all.present?

  @grouped_per_milestone_data_all = {}

  all_recursive_workstream_issues(only_open: false).each do |issue|
    milestone_label = if issue.milestone
      issue.milestone.title
    else
      'no milestone'
    end

    @grouped_per_milestone_data_all[milestone_label] ||= {}
    workflow_label = issue.labels.find {|label| label.start_with?("workflow::")} || 'no workflow label'

    @grouped_per_milestone_data_all[milestone_label][workflow_label] ||= []
    @grouped_per_milestone_data_all[milestone_label][workflow_label] << issue
  end

  @grouped_per_milestone_data_all
end

def epic_title(value)
  "<a href='#{value[:web_url]}' target='_blank'>#{value[:title]}</a>"
end

def issue_title_needs_refactor(issue)
  "<a href='#{issue[:web_url]}' target='_blank'>[ #{issue[:state]} ] #{issue[:title]}</a>"
end

def issue_title(issue)
  "<a href='#{issue.web_url}' target='_blank'>[ #{issue.state} ] #{issue.title}</a>"
end

def count(hash, workflow_column_name)
  if hash[workflow_column_name]
    hash[workflow_column_name].size
  else
    0
  end
end

def pluralize_it(word, count)
  "#{count} #{word.to_s.pluralize(count)}"
end

def count_for_state_needs_refactor(value, state)
  count = 0
  value[:grouped_issues].each do |workflow_label, issues|
    issues_of_interest = issues.select {|i| i[:state] == state.to_s }
    count = count + issues_of_interest.count
  end

  count
end

def count_for_state(issues, state)
  count = 0
  issues_of_interest = issues.select {|i| i.state == state.to_s }
  count = count + issues_of_interest.count
end

def percentage_work_completed(opened_count, closed_count)
  total = opened_count + closed_count
  return 100 if total == 0

  ((closed_count.to_f / total.to_f) * 100.0).round(2)
end

def id_for_element(label, prefix)
  "#{prefix}-#{label}".parameterize
end

def overall_epic_progress
  latest_counts = issue_growth_data.first

  total_count = latest_counts[:total_count]
  closed_count = latest_counts[:closed_count]

  percentage_work_completed((total_count - closed_count), closed_count)
end

def html
  template = %{
  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta name="generator" content="GitLab Pages">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Tenant Scale Progress - Cells 1.0 Issues</title>
      <meta http-equiv="Refresh" content="0; URL=https://epic-dashboard-gitlab-org-tenant-scale-group-4aecf10d1d02154641.gitlab.io/epic_12383" />
      <link rel="stylesheet" href="style.css">
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sortable/0.8.0/css/sortable-theme-bootstrap.css">
      <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/moment@2.29.1/moment.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment@0.1.1"></script>
      <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>

      <script src="https://cdnjs.cloudflare.com/ajax/libs/sortable/0.8.0/js/sortable.min.js"></script>

      <style>
      .content-container {
        width: 85%;
        margin: 0 auto;
        margin-top: 10px;
        margin-bottom: 75px;
      }

      td:not(:first-child) {
        text-align: center;
      }

      th {
        text-align: center;
      }
      </style>

    </head>
    <body>
      <div class="dropdown" style="position: fixed; top: 10px; right: 10px;">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Links
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <!-- Dropdown Items (Links) -->
          <a class="dropdown-item" href="/schema_migration" target="_blank">Schema migration progress</a>
          <a class="dropdown-item" href="/sharding_keys" target="_blank">Sharding key migration progress</a>
          <a class="dropdown-item" href="/cells_10">Cells 1.0 progress</a>
          <a class="dropdown-item" href="https://gitlab.com/gitlab-org/tenant-scale-group/cells-progress-tracker" target="_blank">Contribute</a>
        </div>
      </div>

      <div class="content-container">

        <h1>Progress of issues for Cells 1.0</h1>

        <small>
          This dashboard is used to track issues under the <a href=https://gitlab.com/groups/gitlab-org/-/epics/12383>Cells 1.0 Epic</a>
          <br>
          A scheduled CI pipeline auto-refreshes the data in this dashboard every hour.
          <br>
          Last updated at: <%= Time.now.utc %>
          <br>
          NB: Confidential issues within the epic are excluded from all tables/calculations, except the burnup chart.
          <br>
        </small>

        <br>

        <h3> Overall progress </h3>
          <small> Basis (closed issues count / total issues count) </small>
          <div class="progress w-25" role="progressbar">
            <div class="progress-bar bg-success" style="width: <%=overall_epic_progress%>%"><%= overall_epic_progress %>%</div>
          </div>

          <br>

        <h3> Burnup chart </h3>
          <div style="text-align: center;">
            <div style="width: 600px; height: 400px;">
              <canvas id="issue_growth"></canvas>
            </div>
          </div>

        <h2> Issues, grouped by workflow labels </h2>

        <input type="checkbox" id="toggleTables" onchange="toggleTables()">
        <label for="toggleTables">Include closed issues & epics</label>

        <div id="only-open">
          <div class="workstreams">
            <h3>per child-epic</h3>

            <div style="overflow-x: auto;">
              <table class="sortable-theme-bootstrap" data-sortable>
                <thead>
                  <tr>
                    <th></th> <!-- Empty cell for the corner -->
                    <% workflow_column_names.each do |column_name| %>
                      <th><%= column_name.sub('workflow::', '') %></th>
                    <% end %>
                    <th>Total open</th>
                  </tr>
                </thead>
                <tbody>
                  <% grouped_data.each do |epic_data| %>
                    <% epic_data.each do |epic_iid, value| %>
                      <tr>
                        <td><a href="#<%= id_for_element(epic_iid.to_s, 'workstream') %>"><%= value[:title] %></a></td>
                        <% workflow_column_names.each do |workflow_column_name| %>
                          <% count = count(value[:grouped_issues], workflow_column_name) %>
                          <% if count != 0 %>
                            <td><a href="#<%= id_for_element(workflow_column_name, "workstreams-"+epic_iid.to_s) %>"><%= count  %></a></td>
                          <% else %>
                            <td><%= count %></td>
                          <% end %>
                        <% end %>
                        <td><%= count_for_state_needs_refactor(value, 'opened') %></td>
                      </tr>
                    <% end %>
                  <% end %>
                </tbody>
              </table>
            </div>

            <br>

            <details><summary>Details</summary>
              <ul>
                <% grouped_data.each do |epic_data| %>
                  <% epic_data.each do |epic_iid, value| %>
                    <% next if value[:grouped_issues].count == 0 %>
                    <details><summary id=<%= id_for_element(epic_iid.to_s, 'workstream') %>>
                      <%= epic_title(value) %>
                    </summary>
                      <ul>
                        <% value[:grouped_issues].keys.each do |workflow_label| %>
                          <% next if value[:grouped_issues][workflow_label].count == 0 %>
                          <details><summary id=<%= id_for_element(workflow_label, "workstreams-"+ epic_iid.to_s) %>>
                            <%= workflow_label %> - <%= pluralize_it(:issue, value[:grouped_issues][workflow_label].count) %>
                          </summary>
                            <ul>
                              <% value[:grouped_issues][workflow_label].sort_by {|i| i[:state] }.reverse.each do |issue| %>
                                <li><%= issue_title_needs_refactor(issue) %></li>
                              <% end %>
                            </ul>
                          </details>
                        <% end %>
                      </ul>
                    </details>
                  <% end %>
                <% end %>
              </ul>
            </details>
          </div>

          <br>

          <div class="departments">
            <h3>per department</h3>

            <div style="overflow-x: auto;">
              <table class="sortable-theme-bootstrap" data-sortable>
                <thead>
                  <tr>
                    <th></th> <!-- Empty cell for the corner -->
                    <% workflow_column_names.each do |column_name| %>
                      <th><%= column_name.sub('workflow::', '') %></th>
                    <% end %>
                    <th>Total open</th>
                  </tr>
                </thead>
                <tbody>
                  <% grouped_per_department_data.each do |section_label, hash| %>
                    <% all_issues = hash.values.flatten(1) %>
                    <tr>
                      <td><a href="#<%= id_for_element(section_label, 'section') %>"><%= section_label %></a></td>
                      <% workflow_column_names.each do |workflow_column_name| %>
                        <% count = count(hash, workflow_column_name) %>
                        <% if count != 0 %>
                          <td><a href="#<%= id_for_element(workflow_column_name, section_label) %>"><%= count  %></a></td>
                        <% else %>
                          <td><%= count %></td>
                        <% end %>
                      <% end %>

                      <td><%= count_for_state(all_issues, 'opened') %></td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
            </div>

            <br>

            <details><summary>Details</summary>
              <ul>
                <% grouped_per_department_data.each do |section_label, hash| %>
                  <details><summary id=<%= id_for_element(section_label, 'section') %>>
                    <%= section_label %>
                  </summary>
                    <ul>
                      <% workflow_column_names.each do |workflow_label| %>
                        <% next if hash[workflow_label].blank? || hash[workflow_label].count == 0 %>
                        <details><summary id=<%= id_for_element(workflow_label, section_label) %>>
                          <%= workflow_label %> - <%= pluralize_it(:issue, hash[workflow_label].count) %>
                        </summary>
                          <ul>
                            <% hash[workflow_label].sort_by {|i| i.state }.reverse.each do |issue| %>
                              <li><%= issue_title(issue) %></li>
                            <% end %>
                          </ul>
                        </details>
                      <% end %>
                    </ul>
                  </details>
                <% end %>
              </ul>
            </details>
          </div>

          <br>

          <div class="groups">
            <h3>per team</h3>

            <div style="overflow-x: auto;">
              <table class="sortable-theme-bootstrap" data-sortable>
                <thead>
                  <tr>
                    <th></th> <!-- Empty cell for the corner -->
                    <% workflow_column_names.each do |column_name| %>
                      <th><%= column_name.sub('workflow::', '') %></th>
                    <% end %>
                    <th>Total open</th>
                  </tr>
                </thead>
                <tbody>
                  <% grouped_per_group_data.each do |section_label, hash| %>
                    <% all_issues = hash.values.flatten(1) %>
                    <tr>
                      <td><a href="#<%= id_for_element(section_label, 'group') %>"><%= section_label %></a></td>
                      <% workflow_column_names.each do |workflow_column_name| %>
                        <% count = count(hash, workflow_column_name) %>
                        <% if count != 0 %>
                          <td><a href="#<%= id_for_element(workflow_column_name, section_label) %>"><%= count  %></a></td>
                        <% else %>
                          <td><%= count %></td>
                        <% end %>
                      <% end %>

                      <td><%= count_for_state(all_issues, 'opened') %></td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
            </div>

            <br>

            <details><summary>Details</summary>
              <ul>
                <% grouped_per_group_data.each do |section_label, hash| %>
                  <details><summary id=<%= id_for_element(section_label, 'group') %>>
                    <%= section_label %>
                  </summary>
                    <ul>
                      <% workflow_column_names.each do |workflow_label| %>
                        <% next if hash[workflow_label].blank? || hash[workflow_label].count == 0 %>
                        <details><summary id=<%= id_for_element(workflow_label, section_label) %>>
                          <%= workflow_label %> - <%= pluralize_it(:issue, hash[workflow_label].count) %>
                        </summary>
                          <ul>
                            <% hash[workflow_label].sort_by {|i| i.state }.reverse.each do |issue| %>
                              <li><%= issue_title(issue) %></li>
                            <% end %>
                          </ul>
                        </details>
                      <% end %>
                    </ul>
                  </details>
                <% end %>
              </ul>
            </details>
          </div>

          <br>

          <div class="milestones">
            <h3>per milestone</h3>

            <div style="overflow-x: auto;">
              <table class="sortable-theme-bootstrap" data-sortable>
                <thead>
                  <tr>
                    <th></th> <!-- Empty cell for the corner -->
                    <% workflow_column_names.each do |column_name| %>
                      <th><%= column_name.sub('workflow::', '') %></th>
                    <% end %>
                    <th>Total open</th>
                  </tr>
                </thead>
                <tbody>
                  <% grouped_per_milestone_data.each do |milestone_label, hash| %>
                    <% all_issues = hash.values.flatten(1) %>
                    <tr>
                      <td><a href="#<%= id_for_element(milestone_label, 'milestone') %>"><%= milestone_label %></a></td>
                      <% workflow_column_names.each do |workflow_column_name| %>
                        <% count = count(hash, workflow_column_name) %>
                        <% if count != 0 %>
                          <td><a href="#<%= id_for_element(workflow_column_name, milestone_label) %>"><%= count  %></a></td>
                        <% else %>
                          <td><%= count %></td>
                        <% end %>
                      <% end %>

                      <td><%= count_for_state(all_issues, 'opened') %></td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
            </div>

            <br>

            <details><summary>Details</summary>
              <ul>
                <% grouped_per_milestone_data.each do |milestone_label, hash| %>
                  <details><summary id=<%= id_for_element(milestone_label, 'milestone') %>>
                    <%= milestone_label %>
                  </summary>
                    <ul>
                      <% workflow_column_names.each do |workflow_label| %>
                        <% next if hash[workflow_label].blank? || hash[workflow_label].count == 0 %>
                        <details><summary id=<%= id_for_element(workflow_label, milestone_label) %>>
                          <%= workflow_label %> - <%= pluralize_it(:issue, hash[workflow_label].count) %>
                        </summary>
                          <ul>
                            <% hash[workflow_label].sort_by {|i| i.state }.reverse.each do |issue| %>
                              <li><%= issue_title(issue) %></li>
                            <% end %>
                          </ul>
                        </details>
                      <% end %>
                    </ul>
                  </details>
                <% end %>
              </ul>
            </details>
          </div>
      </div>

      <div id="both-open-and-closed" class="hidden">
          <div class="workstreams">
            <h3>per child-epic</h3>

            <div style="overflow-x: auto;">
              <table class="sortable-theme-bootstrap" data-sortable>
                <thead>
                  <tr>
                    <th></th> <!-- Empty cell for the corner -->
                    <% workflow_column_names.each do |column_name| %>
                      <th><%= column_name.sub('workflow::', '') %></th>
                    <% end %>
                    <th>Total open</th>
                    <th>Total closed</th>
                    <th>% Work complete</th>
                  </tr>
                </thead>
                <tbody>
                  <% grouped_data_all.each do |epic_data| %>
                    <% epic_data.each do |epic_iid, value| %>
                      <tr>
                        <td><a href="#<%= id_for_element(epic_iid.to_s, 'workstream-all') %>"><%= value[:title] %></a></td>
                        <% workflow_column_names.each do |workflow_column_name| %>
                          <% count = count(value[:grouped_issues], workflow_column_name) %>
                          <% if count != 0 %>
                            <td><a href="#<%= id_for_element(workflow_column_name, "workstreams-"+epic_iid.to_s+"-all") %>"><%= count  %></a></td>
                          <% else %>
                            <td><%= count %></td>
                          <% end %>
                        <% end %>
                        <td><%= count_for_state_needs_refactor(value, 'opened') %></td>
                        <td><%= count_for_state_needs_refactor(value, 'closed') %></td>
                        <td><%= percentage_work_completed(count_for_state_needs_refactor(value, 'opened'), count_for_state_needs_refactor(value, 'closed')) %>%</td>
                      </tr>
                    <% end %>
                  <% end %>
                </tbody>
              </table>
            </div>

            <br>

            <details><summary>Details</summary>
              <ul>
                <% grouped_data_all.each do |epic_data| %>
                  <% epic_data.each do |epic_iid, value| %>
                    <% next if value[:grouped_issues].count == 0 %>
                    <details><summary id=<%= id_for_element(epic_iid.to_s, 'workstream-all') %>>
                      <%= epic_title(value) %>
                    </summary>
                      <ul>
                        <% value[:grouped_issues].keys.each do |workflow_label| %>
                          <% next if value[:grouped_issues][workflow_label].count == 0 %>
                          <details><summary id=<%= id_for_element(workflow_label, "workstreams-"+ epic_iid.to_s+"-all") %>>
                            <%= workflow_label %> - <%= pluralize_it(:issue, value[:grouped_issues][workflow_label].count) %>
                          </summary>
                            <ul>
                              <% value[:grouped_issues][workflow_label].sort_by {|i| i[:state] }.reverse.each do |issue| %>
                                <li><%= issue_title_needs_refactor(issue) %></li>
                              <% end %>
                            </ul>
                          </details>
                        <% end %>
                      </ul>
                    </details>
                  <% end %>
                <% end %>
              </ul>
            </details>
          </div>

          <br>

          <div class="departments">
            <h3>per department</h3>

            <div style="overflow-x: auto;">
              <table class="sortable-theme-bootstrap" data-sortable>
                <thead>
                  <tr>
                    <th></th> <!-- Empty cell for the corner -->
                    <% workflow_column_names.each do |column_name| %>
                      <th><%= column_name.sub('workflow::', '') %></th>
                    <% end %>
                    <th>Total open</th>
                    <th>Total closed</th>
                    <th>% Work complete</th>
                  </tr>
                </thead>
                <tbody>
                  <% grouped_per_department_data_all.each do |section_label, hash| %>
                    <% all_issues = hash.values.flatten(1) %>
                    <tr>
                      <td><a href="#<%= id_for_element(section_label, 'section-all') %>"><%= section_label %></a></td>
                      <% workflow_column_names.each do |workflow_column_name| %>
                        <% count = count(hash, workflow_column_name) %>
                        <% if count != 0 %>
                          <td><a href="#<%= id_for_element(workflow_column_name, section_label+"-all") %>"><%= count  %></a></td>
                        <% else %>
                          <td><%= count %></td>
                        <% end %>
                      <% end %>

                      <td><%= count_for_state(all_issues, 'opened') %></td>
                      <td><%= count_for_state(all_issues, 'closed') %></td>
                      <td><%= percentage_work_completed(count_for_state(all_issues, 'opened'), count_for_state(all_issues, 'closed')) %>%</td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
            </div>

            <br>

            <details><summary>Details</summary>
              <ul>
                <% grouped_per_department_data_all.each do |section_label, hash| %>
                  <details><summary id=<%= id_for_element(section_label, 'section-all') %>>
                    <%= section_label %>
                  </summary>
                    <ul>
                      <% workflow_column_names.each do |workflow_label| %>
                        <% next if hash[workflow_label].blank? || hash[workflow_label].count == 0 %>
                        <details><summary id=<%= id_for_element(workflow_label, section_label+"-all") %>>
                          <%= workflow_label %> - <%= pluralize_it(:issue, hash[workflow_label].count) %>
                        </summary>
                          <ul>
                            <% hash[workflow_label].sort_by {|i| i.state }.reverse.each do |issue| %>
                              <li><%= issue_title(issue) %></li>
                            <% end %>
                          </ul>
                        </details>
                      <% end %>
                    </ul>
                  </details>
                <% end %>
              </ul>
            </details>
          </div>

          <br>

          <div class="groups">
            <h3>per team</h3>

            <div style="overflow-x: auto;">
              <table class="sortable-theme-bootstrap" data-sortable>
                <thead>
                  <tr>
                    <th></th> <!-- Empty cell for the corner -->
                    <% workflow_column_names.each do |column_name| %>
                      <th><%= column_name.sub('workflow::', '') %></th>
                    <% end %>
                    <th>Total open</th>
                    <th>Total closed</th>
                    <th>% Work complete</th>
                  </tr>
                </thead>
                <tbody>
                  <% grouped_per_group_data_all.each do |section_label, hash| %>
                    <% all_issues = hash.values.flatten(1) %>
                    <tr>
                      <td><a href="#<%= id_for_element(section_label, 'group-all') %>"><%= section_label %></a></td>
                      <% workflow_column_names.each do |workflow_column_name| %>
                        <% count = count(hash, workflow_column_name) %>
                        <% if count != 0 %>
                          <td><a href="#<%= id_for_element(workflow_column_name, section_label+"-all") %>"><%= count  %></a></td>
                        <% else %>
                          <td><%= count %></td>
                        <% end %>
                      <% end %>

                      <td><%= count_for_state(all_issues, 'opened') %></td>
                      <td><%= count_for_state(all_issues, 'closed') %></td>
                      <td><%= percentage_work_completed(count_for_state(all_issues, 'opened'), count_for_state(all_issues, 'closed')) %>%</td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
            </div>

            <br>

            <details><summary>Details</summary>
              <ul>
                <% grouped_per_group_data_all.each do |section_label, hash| %>
                  <details><summary id=<%= id_for_element(section_label, 'group-all') %>>
                    <%= section_label %>
                  </summary>
                    <ul>
                      <% workflow_column_names.each do |workflow_label| %>
                        <% next if hash[workflow_label].blank? || hash[workflow_label].count == 0 %>
                        <details><summary id=<%= id_for_element(workflow_label, section_label+"-all") %>>
                          <%= workflow_label %> - <%= pluralize_it(:issue, hash[workflow_label].count) %>
                        </summary>
                          <ul>
                            <% hash[workflow_label].sort_by {|i| i.state }.reverse.each do |issue| %>
                              <li><%= issue_title(issue) %></li>
                            <% end %>
                          </ul>
                        </details>
                      <% end %>
                    </ul>
                  </details>
                <% end %>
              </ul>
            </details>
          </div>

          <br>

          <div class="milestones">
            <h3>per milestone</h3>

            <div style="overflow-x: auto;">
              <table class="sortable-theme-bootstrap" data-sortable>
                <thead>
                  <tr>
                    <th></th> <!-- Empty cell for the corner -->
                    <% workflow_column_names.each do |column_name| %>
                      <th><%= column_name.sub('workflow::', '') %></th>
                    <% end %>
                    <th>Total open</th>
                    <th>Total closed</th>
                    <th>% Work complete</th>
                  </tr>
                </thead>
                <tbody>
                  <% grouped_per_milestone_data_all.each do |milestone_label, hash| %>
                    <% all_issues = hash.values.flatten(1) %>
                    <tr>
                      <td><a href="#<%= id_for_element(milestone_label, 'milestone-all') %>"><%= milestone_label %></a></td>
                      <% workflow_column_names.each do |workflow_column_name| %>
                        <% count = count(hash, workflow_column_name) %>
                        <% if count != 0 %>
                          <td><a href="#<%= id_for_element(workflow_column_name, milestone_label+"-all") %>"><%= count  %></a></td>
                        <% else %>
                          <td><%= count %></td>
                        <% end %>
                      <% end %>

                      <td><%= count_for_state(all_issues, 'opened') %></td>
                      <td><%= count_for_state(all_issues, 'closed') %></td>
                      <td><%= percentage_work_completed(count_for_state(all_issues, 'opened'), count_for_state(all_issues, 'closed')) %>%</td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
            </div>

            <br>

            <details><summary>Details</summary>
              <ul>
                <% grouped_per_milestone_data_all.each do |milestone_label, hash| %>
                  <details><summary id=<%= id_for_element(milestone_label, 'milestone-all') %>>
                    <%= milestone_label %>
                  </summary>
                    <ul>
                      <% workflow_column_names.each do |workflow_label| %>
                        <% next if hash[workflow_label].blank? || hash[workflow_label].count == 0 %>
                        <details><summary id=<%= id_for_element(workflow_label, milestone_label+"-all") %>>
                          <%= workflow_label %> - <%= pluralize_it(:issue, hash[workflow_label].count) %>
                        </summary>
                          <ul>
                            <% hash[workflow_label].sort_by {|i| i.state }.reverse.each do |issue| %>
                              <li><%= issue_title(issue) %></li>
                            <% end %>
                          </ul>
                        </details>
                      <% end %>
                    </ul>
                  </details>
                <% end %>
              </ul>
            </details>
          </div>
      </div>

      </div>

      <script>
        var dates = <%= issue_growth_data.map {|e| e[:date]}.to_json %>
        var total_count = <%= issue_growth_data.map {|e| e[:total_count]}.to_json %>
        var closed_count = <%= issue_growth_data.map {|e| e[:closed_count]}.to_json %>

        var issue_growth_ctx = document.getElementById("issue_growth").getContext("2d");

        const myChart = new Chart(issue_growth_ctx, {
            type: "line",
            data: {
                labels: dates, // X-axis data (dates)
                datasets: [{
                    label: "Total issues",
                    data: total_count, // Y-axis data (counts)
                    borderColor: "rgb(0, 0, 255)", // Line color
                    fill: false, // Don't fill the area under the line
                },
                {
                    label: "Closed issues",
                    data: closed_count, // Y-axis data (counts)
                    borderColor: "rgb(0, 255, 0)", // Line color
                    fill: false, // Don't fill the area under the line
                }]
            },
            options: {
                scales: {
                    x: {
                        type: 'time',
                        time: {
                            unit: 'month',
                            displayFormats: {
                              month: 'MMM YYYY' // Customize the date format
                          },
                        },
                    },
                    y: {
                        beginAtZero: false,
                    }
                }
            }
        });


        function toggleTables() {
          var checkbox = document.getElementById("toggleTables");
          var only_open = document.getElementById("only-open");
          var both_open_and_closed = document.getElementById("both-open-and-closed");

          if (checkbox.checked) {
            only_open.classList.add("hidden");
            both_open_and_closed.classList.remove("hidden");
          } else {
            only_open.classList.remove("hidden");
            both_open_and_closed.classList.add("hidden");
          }
        }
      </script>
    </body>
  </html>
  }.gsub(/^  /, '')

  rhtml = ERB.new(template)
  rhtml.result
end


File.open('public/cells_10.html', 'w+') { |file| file.write(html) }
