require "bundler/inline"
require 'net/http'
require 'uri'
require 'json'

gemfile do
  source "https://rubygems.org"
  gem "gitlab"
end

Gitlab.configure do |config|
  config.endpoint      = "https://gitlab.com/api/v4"
  config.private_token = ENV["GITLAB_API_PRIVATE_TOKEN"]
end

class Gitlab::Client
  module Epics
    def epic_children(group_id, epic_iid)
      get("/groups/#{group_id}/epics/#{epic_iid}/epics")
    end
  end
end

#!/usr/bin/env ruby

def existing_counts
  latest_issue_count = File.foreach('scripts/cells_10/issue-count-growth.txt').first
  date, counts = latest_issue_count.split(': ')
  counts = counts.split(",")

  [counts[0].to_i, counts[1].to_i]
end

def file_prepend(file, str)
  new_contents = ""
  File.open(file, 'r') do |fd|
    contents = fd.read
    new_contents = str << contents
  end
  # Overwrite file but now with prepended string on it
  File.open(file, 'w') do |fd|
    fd.write(new_contents)
  end
end

def write_new_count(closed, total)
  date = Time.now.strftime("%Y-%m-%d")

  value = "#{date}: #{closed}, #{total}"

  if existing_counts != [closed, total]
    file_prepend("scripts/cells_10/issue-count-growth.txt", "#{value}\n")
  end
end


# 
class Issue
  GROUP_ID = 9970 # https://gitlab.com/gitlab-org
  EPIC_IID = 12383 # https://gitlab.com/groups/gitlab-org/-/epics/12383

  def save_issue_counts
   # all_issues = []

   # process(all_issues, GROUP_ID, EPIC_IID)

   # closed_issues = all_issues.select { |issue| issue.state == "closed" }

    closed_count, opened_count = get_counts_via_graphql
    total_count = closed_count + opened_count

    write_new_count(closed_count, total_count)


    # issues_by_group = Hash.new { |h, k| h[k] = [] }
    # issues.each do |issue|
    #   group_label = (issue.labels.find { |label| label.start_with?("group::") } || "undefined")
    #   issues_by_group[group_label] << issue
    # end

    # issues_by_group.each do |group, issues|
    #   puts "#{group}: #{issues.size}"
    # end
  end

  private

  def graphql_query
    <<~GRAPHQL
      query {
        group(fullPath: "gitlab-org") {
          epic(iid: 12383) {
            descendantCounts {
              closedIssues
              openedIssues
            }
          }
        }
      }
    GRAPHQL
  end

  def get_counts_via_graphql
    url = URI("https://gitlab.com/api/graphql")
    request = Net::HTTP::Post.new(url)
    request["Content-Type"] = "application/json"
    request.body = JSON.dump({
      query: graphql_query
    })

    response = Net::HTTP.start(url.host, url.port, use_ssl: true) do |http|
      http.request(request)
    end

    if response.is_a?(Net::HTTPSuccess)
      result = JSON.parse(response.body)
      epic = result.dig("data", "group", "epic")

      if epic
        descendant_counts = epic["descendantCounts"]

        closed_count = descendant_counts["closedIssues"]
        opened_count = descendant_counts["openedIssues"]

        [closed_count, opened_count]
      else
        puts "Epic not found"
      end
    else
      puts "Error: #{response.code} - #{response.message}"
    end
  end

  # def process(all_issues, group_id, epic_iid)
  #   add_issues_to_list(all_issues, group_id, epic_iid)

  #   child_epics = Gitlab.epic_children(group_id, epic_iid).auto_paginate

  #   child_epics.each do |epic|
  #     process(all_issues, epic.group_id, epic.iid)
  #   end
  # end

  # def add_issues_to_list(all_issues, group_id, epic_iid)
  #   issues = Gitlab.epic_issues(group_id, epic_iid).auto_paginate

  #   issues.each { |i| all_issues << i }
  # end
end

Issue.new.save_issue_counts
