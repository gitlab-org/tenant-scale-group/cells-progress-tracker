require "bundler/inline"
require 'net/http'
require 'uri'
require 'json'
require 'active_support/all'

gemfile do
  source "https://rubygems.org"
  gem "gitlab"
  gem "activesupport"
end

GROUP_ID = 9970 # https://gitlab.com/gitlab-org
EPIC_IID = 12383 # https://gitlab.com/groups/gitlab-org/-/epics/12383


Gitlab.configure do |config|
  config.endpoint      = "https://gitlab.com/api/v4"
  config.private_token = ENV["GIT_PUSH_TOKEN"]
end

class Gitlab::Client
  module Epics
    def epic_children(group_id, epic_iid)
      get("/groups/#{group_id}/epics/#{epic_iid}/epics")
    end
  end
end

class FirstLevelEpic
  attr_reader :data, :self_data, :group_id, :epic_iid

  def initialize(group_id, epic_iid)
    @self_data = Gitlab.epic(group_id, epic_iid)
    @children_data = Gitlab.epic_children(group_id, epic_iid).auto_paginate
    @group_id = group_id
    @epic_iid = epic_iid
  end


  def title
    self_data.title
  end

  def iid
    self_data.iid
  end

  def web_url
    self_data.web_url
  end

  def state
    self_data.state
  end

  def confidential
    self_data.confidential
  end

  def issues(only_open: true)
    if only_open
      @issues_only_open ||= Gitlab.epic_issues(group_id, epic_iid).auto_paginate.select {|i| i.state == 'opened' }
    else
      @issues ||= Gitlab.epic_issues(group_id, epic_iid).auto_paginate
    end

    only_open ?  @issues_only_open : @issues
  end

  def workstream_epics(only_open: true)
    @workstream_epics_data = @children_data.map {|d| WorkStreamEpic.new(d) }

    if only_open
      @workstream_epics_data_open ||= @workstream_epics_data.select { |e| e.open? }
    else
      @workstream_epics_data_all ||= @workstream_epics_data
    end

    only_open ? @workstream_epics_data_open : @workstream_epics_data_all
  end

  def all_recursive_workstream_issues(only_open: true)
    result = []

    recursive_issues_from_children = workstream_epics(only_open: only_open).map {|e| e.recursive_issues(only_open: only_open) }

    result << recursive_issues_from_children.flatten(1)

    issues(only_open: only_open).each { |i| result << i }

    result.flatten(1)
  end
end


class WorkStreamEpic
  include Enumerable
  attr_reader :data

  delegate :each, to: :data

  def initialize(data)
    @data = data
    @result = []
    @result_all = []
  end

  def title
    data.title
  end

  def iid
    data.iid
  end

  def web_url
    data.web_url
  end

  def state
    data.state
  end

  def confidential
    data.confidential
  end

  def closed?
    state == 'closed'
  end

  def open?
    state == 'opened'
  end

  def recursive_issues(only_open: true)
    if only_open
      @open_data ||= process(@result, data.group_id, data.iid, only_open: only_open)
    else
      @all_data ||=process(@result_all, data.group_id, data.iid, only_open: only_open)
    end

    only_open ?  @result : @result_all
  end

  private

  def process(all_issues, group_id, epic_iid, only_open: true)
    add_issues_to_list(all_issues, group_id, epic_iid, only_open: only_open)

    child_epics = Gitlab.epic_children(group_id, epic_iid).auto_paginate

    child_epics.each do |epic|
      process(all_issues, epic.group_id, epic.iid, only_open: only_open)
    end
  end

  def add_issues_to_list(all_issues, group_id, epic_iid, only_open: true)
    issues = Gitlab.epic_issues(group_id, epic_iid).auto_paginate

    # only select open issues
    issues = issues.select {|i| i.state == 'opened' } if only_open

    issues.each { |i| all_issues << i  }
  end
end

def cells_1_epic
  @cells_1_epic ||= FirstLevelEpic.new(GROUP_ID, EPIC_IID)
end


def grouped_issues_data(only_open: true)
  all_data = []

  # add issues in the root level epic

  self_issues_data = cells_1_epic.issues(only_open: only_open).map do |i|
    { title: i.title, labels: i.labels, state: i.state, web_url: i.web_url }
  end

  all_data << { cells_1_epic.iid  => { title: cells_1_epic.title + " (self)", web_url: cells_1_epic.web_url, state: cells_1_epic.state,  issues: self_issues_data } }

  cells_1_epic.workstream_epics(only_open: only_open).each do |workstream_epic|
    issue_data = workstream_epic.recursive_issues(only_open: only_open).map do |i|
      { title: i.title, labels: i.labels, state: i.state, web_url: i.web_url }
    end

    all_data << { workstream_epic.iid  => { title: workstream_epic.title, web_url: workstream_epic.web_url, state: workstream_epic.state,  issues: issue_data } }
  end

  all_data.each do |epic_data|
    epic_data.each do |epic_iid, value|
      issues = value[:issues]
      grouped_issues = Hash.new { |h, k| h[k] = [] }

      issues.each do |issue|
        if (workflow_label = issue[:labels].find {|label| label.start_with?("workflow::")})
          grouped_issues[workflow_label] << issue
        else
          grouped_issues['no workflow label'] << issue
        end
      end

      value[:grouped_issues] = grouped_issues
    end
  end

  all_data
end

def all_recursive_workstream_issues(only_open: true)
  if only_open
    @all_recursive_workstream_issues_only_open ||= cells_1_epic.all_recursive_workstream_issues(only_open: only_open)
  else
    @all_recursive_workstream_issues ||= cells_1_epic.all_recursive_workstream_issues(only_open: only_open)
  end

  only_open ? @all_recursive_workstream_issues_only_open : @all_recursive_workstream_issues
end


