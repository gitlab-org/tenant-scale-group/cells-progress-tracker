require 'yaml'
require 'erb'

Dir.chdir "gitlab"

def data
  @data ||= Dir.glob("db/docs/*.yml").map do |file|
    YAML.load_file(file)
  end.group_by{ |data| data['gitlab_schema'] }
end

def gitlab_main_cell_tables
  @gitlab_main_cell_tables ||= data['gitlab_main_cell'].filter_map { |d| d['table_name'] unless d['exempt_from_sharding'] == true }
end

def gitlab_main_clusterwide_tables
  @gitlab_main_clusterwide_tables ||= data['gitlab_main_clusterwide'].filter_map { |d| d['table_name'] unless d['exempt_from_sharding'] == true }
end

def gitlab_main_tables
  @gitlab_main_tables ||= data['gitlab_main'].filter_map { |d| d['table_name'] unless d['exempt_from_sharding'] == true }
end

def total_size
  gitlab_main_cell_tables.count + gitlab_main_clusterwide_tables.count + gitlab_main_tables.count
end

Dir.chdir "."

puts total_size
